import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeReactiveFormsComponent } from './employee-reactive-forms.component';

describe('EmployeeReactiveFormsComponent', () => {
  let component: EmployeeReactiveFormsComponent;
  let fixture: ComponentFixture<EmployeeReactiveFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeReactiveFormsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeReactiveFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
