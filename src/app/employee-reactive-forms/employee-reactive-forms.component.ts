import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeListService } from '../employee-list.service';
import { ModalServiceComponent } from '../modal-service/modal-service.component';

@Component({
  selector: 'app-employee-reactive-forms',
  templateUrl: './employee-reactive-forms.component.html',
  styleUrls: ['./employee-reactive-forms.component.css']
})
export class EmployeeReactiveFormsComponent implements OnInit {
  public action:any;
  public response:any;
  public employees:any=[];
  registrationForm!:FormGroup;
  @Output() childEvent=new EventEmitter();
  constructor(private _employeeService:EmployeeListService,private modalService:NgbModal,private fb:FormBuilder) { }

  ngOnInit(): void {
    this._employeeService.getEmployeelist()
    .subscribe(data => this.employees = data);  
  }
  Mopen(employee?:any){
    if(typeof(employee) == 'undefined'){
      this.action=true;
    }
    else{
      var index = this.employees.findIndex((x: { id: any; })=>x.id ==employee.id);
      this.action=false;
    }
    // this.registrationForm.markAsUntouched();
    // this.modalService.open(content);

    const mref=this.modalService.open(ModalServiceComponent);
    mref.componentInstance.employee=employee;
    mref.componentInstance.action=this.action;
    mref.result.then(response=>{
        if(response!=undefined){
        if(this.action){
          this.employees.push(response)
          this._employeeService.sendMessage(this.employees.length);
          this.childEvent.emit('Add');
        }
        else{
          this.employees[index].name=response.name;
          this.employees[index].username=response.username;
          this.employees[index].email=response.email;
          console.log(response);
          this.childEvent.emit('Edit');
        }
      }
    });
   
    
  }
  del(r: any){
  
    this._employeeService.delRes(r)
     .subscribe(data=> {console.log(data),this.response=data},error=>{console.log(error)});
     this.employees.splice(r,1);
     this.childEvent.emit('Delete');
     this._employeeService.sendMessage(this.employees.length);
  }
}
