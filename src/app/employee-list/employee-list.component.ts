import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeListService } from '../employee-list.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  public response:any;
  public employees:any=[];
  public closeResult = '';
  public result:any;
  public eid:any;
  public title:any;
  public action:any;
  public employeeForm:any;
  public entry = {
    "id" :"",
    "name" : "",
    "username" : "",
    "email" : ""
    
  };
  en: any;
  
  constructor(private _employeeService:EmployeeListService,private modalService:NgbModal) { }

  ngOnInit(): void {
    this._employeeService.getEmployeelist()
      .subscribe(data => this.employees = data);
     
     
  }
  Mopen(content:any,e?:any){
    if(typeof(e) == 'undefined'){
      this.action=true;
      this.title="Add";
      this.entry={
        "id" :"",
        "name" : "",
        "username" : "",
        "email" : ""
        
      };
      this.modalService.open(content);
    }
    else{
      this.eid=e;
      this.action=false;
      this.title="Edit";
      this.entry.id=this.employees[e].id;
      this.entry.name=this.employees[e].name;
      this.entry.username=this.employees[e].username;
      this.entry.email=this.employees[e].email;
      this.modalService.open(content);
    }
  }

   del(r: any){
  
   this._employeeService.delRes(r)
    .subscribe(data=> {console.log(data),this.response=data},error=>{console.log(error)});
    
    //this.employees.splice(r, 1);
 
}
addEmployee(modal:any){

this._employeeService.addRes(this.entry)
  .subscribe(data=> {console.log(data);this.response=data},error=> {console.log(error)});
 
//this.employees.push(pentry);

modal.dismiss();
}
editEmployee(modal:any){
 
 this._employeeService.upRes(this.entry)
  .subscribe(data => console.log(data),error=> console.log(error)); 

 modal.dismiss();
}
demo(){

  

}
  

}
