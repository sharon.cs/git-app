import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  getEmployees(){
    return [
      {"id":1,"name":"sharon","age":23,"des":"se"},
      {"id":2,"name":"stanly","age":23,"des":"se"},
      {"id":3,"name":"samuel","age":23,"des":"se"}
  ];
  }

}
