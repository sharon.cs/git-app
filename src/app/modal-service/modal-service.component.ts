import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeListService } from '../employee-list.service';

@Component({
  selector: 'app-modal-service',
  templateUrl: './modal-service.component.html',
  styleUrls: ['./modal-service.component.css']
})

export class ModalServiceComponent implements OnInit {
  registrationForm!:FormGroup;
  @Input()employee:any;
  @Input()action:any;
  public response:any;
  constructor(public activeModal: NgbActiveModal,private _employeeService:EmployeeListService,private modalService:NgbModal,private fb:FormBuilder) { }

  ngOnInit(): void {
    this.registrationForm=this.fb.group({
      id:[this.employee?this.employee.id:''],
      name:[this.employee?this.employee.name:'',[Validators.required,Validators.pattern("[a-zA-z ]*")]],
      username:[this.employee?this.employee.username:'',Validators.required],
      email:[this.employee?this.employee.email:'',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]]
    })
    this.registrationForm.controls['id'].disable();
  }
  addEmployee(modal?:any){

    this._employeeService.addRes(this.registrationForm.value)
      .subscribe(data=> {console.log(data);this.activeModal.close(data)},
      error=> {console.log(error)});
   
  }
  editEmployee(modal?:any){
 
    this._employeeService.upRes(this.registrationForm.value)
      .subscribe(data => {console.log(data);this.activeModal.close(this.registrationForm.value)},error=> console.log(error))
   
  }


}
