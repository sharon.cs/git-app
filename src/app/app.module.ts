import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeService } from './employee.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeListService } from './employee-list.service';
import {HttpClientModule} from '@angular/common/http';
import { EmployeeReactiveFormsComponent } from './employee-reactive-forms/employee-reactive-forms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalServiceComponent } from './modal-service/modal-service.component';
import { MaincomponentComponent } from './maincomponent/maincomponent.component';
import { HeadercomponentComponent } from './headercomponent/headercomponent.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeDetailsComponent,
    EmployeeListComponent,
    EmployeeReactiveFormsComponent,
    ModalServiceComponent,
    MaincomponentComponent,
    HeadercomponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [EmployeeService,EmployeeListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
