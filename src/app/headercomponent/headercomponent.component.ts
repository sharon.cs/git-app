import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EmployeeListService } from '../employee-list.service';

@Component({
  selector: 'app-headercomponent',
  templateUrl: './headercomponent.component.html',
  styleUrls: ['./headercomponent.component.css']
})
export class HeadercomponentComponent implements OnInit {
  public empcount:any;
  @Input() empnumber:any;
  constructor(private _employeeService:EmployeeListService) { }

  ngOnInit(): void {
    this._employeeService.subject.subscribe(data=>this.empcount=data)
  }
}
