import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IEmployee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeListService {
  public subject=new BehaviorSubject<any>(10);
  private _url:string="https://jsonplaceholder.typicode.com/users";
  public _urlT=this._url;
  constructor(private http:HttpClient) { }
  // getEmployeelist():Observable<IEmployee[]>{
  //   return this.http.get<IEmployee[]>(this._url);
  sendMessage(empcount:any){
    this.subject.next(empcount);
  }
  receiveMessage():Observable<any>{
    return this.subject.asObservable();
  }
  getEmployeelist(){
    return this.http.get(this._url);
  }
  addRes(pentry:any){
    return this.http.post(this._url,pentry);
     
  }
  upRes(pentry:any){
    this._urlT=this._url+"/"+pentry.id;
   return this.http.patch(this._urlT,pentry);
  }
  delRes(r:any){
    this._urlT=this._url+"/"+r;
return this.http.delete(this._urlT);

  }
  }


