import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDismissReasons ,NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

 
  public employees:any=[];
  public closeResult = '';
  public eid:any;
  public title:any;
  public action:any;
  public entry = {
    "id" :"",
    "name" : "",
    "age" : "",
    "des" : ""
    
  };
  en: any;
 
  constructor(private _employeeService :EmployeeService ,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.employees=this._employeeService.getEmployees();
  }

  Mopen(content:any,e?:any){
    if(typeof(e) == 'undefined'){
      this.action=true;
      this.title="Add";
      this.entry={
        "id" :"",
        "name" : "",
        "age" : "",
        "des" : ""
        
      };
      this.modalService.open(content);
    }
    else{
      this.eid=e;
      this.action=false;
      this.title="Edit";
      this.entry.id=this.employees[e].id;
      this.entry.name=this.employees[e].name;
      this.entry.age=this.employees[e].age;
      this.entry.des=this.employees[e].des;
      this.modalService.open(content);
    }
  }

   del(r: any){
   // alert(r);
    this.employees.splice(r, 1);
 
}
addEmployee(modal:any){
  var pentry = {
    "id" : this.entry.id,
    "name" : this.entry.name,
    "age" : this.entry.age,
    "des" : this.entry.des
  };
this.employees.push(pentry);

modal.dismiss();
}
editEmployee(modal:any){
 this.employees[this.eid].id=this.entry.id;
 this.employees[this.eid].name=this.entry.name;
 this.employees[this.eid].age=this.entry.age;
 this.employees[this.eid].des=this.entry.des;
 modal.dismiss();
}


}



