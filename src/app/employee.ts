export interface IEmployee{
    id:string,
    name:string,
    username:string,
    email:string
}