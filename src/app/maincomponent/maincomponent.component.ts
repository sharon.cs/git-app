import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maincomponent',
  templateUrl: './maincomponent.component.html',
  styleUrls: ['./maincomponent.component.css']
})
export class MaincomponentComponent implements OnInit {
  public empnumber="Total Number Of Employees";
  public data:any;
  constructor() { }

  ngOnInit(): void {
  }

}
